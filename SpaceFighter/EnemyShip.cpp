
#include "EnemyShip.h"


EnemyShip::EnemyShip()
{
	SetMaxHitPoints(1);
	SetCollisionRadius(20);
}

//Detailed Documentation Three
//Used to keep track and update the status of a EnemyShip object.
void EnemyShip::Update(const GameTime *pGameTime)
{
	//This if-statement checks if m_delaySeconds is greater than zero, meaning that delay needs to be calculated.
	if (m_delaySeconds > 0)
	{
		//Updates time elapsed by using m_delaySecond's original value, and subtracting that against how much time has passed. Will count down to zero.
		m_delaySeconds -= pGameTime->GetTimeElapsed();

		/*This if - statement is used to check when m_delaySeconds reaches a value of zero AFTER being activated of being above a
		zero, meaning when it is detected to be less than zero here, it is being used as a countdown.*/
		if (m_delaySeconds <= 0)
		{
			//Activates the GameObject with a Activate function.
			GameObject::Activate();
		}
	}

	//This if-statement will run if the instance of the EnemyShip object is currently enabled.
	if (IsActive())
	{
		//Tracks how long the object has been activated for by adding elapsedTime onto the m_activationSeconds.
		m_activationSeconds += pGameTime->GetTimeElapsed();
		
		//Deactivation of the object occurs when m_activationSeconds is above a value of 2, and object is not on screen.
		if (m_activationSeconds > 2 && !IsOnScreen()) Deactivate();
	}

	//Takes new data and executes an update statement to apply that data to the Ship object, replacing the old data.
	Ship::Update(pGameTime);
}


void EnemyShip::Initialize(const Vector2 position, const double delaySeconds)
{
	SetPosition(position);
	m_delaySeconds = delaySeconds;

	Ship::Initialize();
}


void EnemyShip::Hit(const float damage)
{
	Ship::Hit(damage);
}